
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	let firstName = "Noctis Caelum";
	let lastName = "Lucis";
	let myAge = 28;
	let hobbies = ["Sword Duels", "Travelling", "Running"];
	let workAddress = {
		houseNumber: 55,
		street: "Illuminati St.",
		city: "Angel City",
		state: "Lucis State"
	}

	//Add your variables and console log for objective 1 here:
	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: "+ myAge);
	console.log("Hobbies: " + "\n");
	console.log(hobbies);
	console.log("Work Address: "+ "\n");
	console.log(workAddress);



/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: " + "\n");
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: " + "\n");
	console.log(profile);

	let myBestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + myBestFriend);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
